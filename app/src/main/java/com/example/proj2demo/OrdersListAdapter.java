package com.example.proj2demo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.ViewHolder> {

    public OnClickOrdersList onClickOrdersList = null;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view holder using the cell order XML layout
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Ensure position value is valid to avoid index out of range errors
        if ((position < 0) || (position >= orders.size())) {
            System.out.println("Error: position " + position + " in OrdersListAdapter is invalid.");
            return;
        }

        Order order = orders.get(position);
        String positionString = String.valueOf(position + 1);
        holder.itemNumberTextView.setText(positionString);
        holder.orderNameTextView.setText(order.name);
        String dollarString = String.format("$%,.2f", Double.valueOf(order.priceInCents + order.taxInCents) / 100.0);
        holder.orderPriceTextView.setText(dollarString);
        holder.moreImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if ((onClickOrdersList != null) && (position >= 0) && (position < orders.size())) {
                    onClickOrdersList.onClickOrder(position, orders.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    interface OnClickOrdersList {
        void onClickOrder(int position, Order order);
    }

    // Constructor
    OrdersListAdapter(ArrayList<Order> orders) {
        this.orders = orders;
    }

    private ArrayList<Order> orders;

    // View holder class that corresponds to order cell XML layout
    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemNumberTextView;
        TextView orderNameTextView;
        TextView orderPriceTextView;
        ImageButton moreImageButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            // Connect XML views for order cell
            itemNumberTextView = itemView.findViewById(R.id.textView_item_number);
            orderNameTextView = itemView.findViewById(R.id.textView_order_name);
            orderPriceTextView = itemView.findViewById(R.id.textView_order_price);
            moreImageButton = itemView.findViewById(R.id.imageButton_more);
        }
    }
}
