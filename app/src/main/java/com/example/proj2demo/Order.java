package com.example.proj2demo;

import java.util.ArrayList;

public class Order {

    String name;
    int priceInCents;
    int taxInCents;

    static ArrayList<Order> orders = new ArrayList<>();

    Order(String name, int priceInCents, int taxInCents) {
        this.name = name;
        this.priceInCents = priceInCents;
        this.taxInCents = taxInCents;
    }
}
