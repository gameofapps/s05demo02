package com.example.proj2demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class SplitBillActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_bill);

        // Retrieve parameters from Main Activity
        Intent intent = getIntent();
        billTotalInCents = intent.getIntExtra("billTotalInCents", 0);

        splitIntoNumber = (Order.orders.size() > 0) ? Order.orders.size() : 1;
        setupDisplay();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private int billTotalInCents = 0;
    private int splitIntoNumber = 1;

    private void setupDisplay() {
        Button doneButton = findViewById(R.id.button_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton subtractButton = findViewById(R.id.button_subtract);
        subtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (splitIntoNumber > 1) {
                    splitIntoNumber--;
                    updateSplitNumber();
                    updateSplitAmount();
                }
            }
        });

        ImageButton addButton = findViewById(R.id.button_add);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                splitIntoNumber++;
                updateSplitNumber();
                updateSplitAmount();
            }
        });

        displayTotal();
        updateSplitNumber();
        updateSplitAmount();
    }

    private void displayTotal() {
        TextView totalTextView = findViewById(R.id.textView_total);
        double totalDouble = (double) billTotalInCents / 100.0;
        String totalString = String.format("$%,.2f", totalDouble);
        totalTextView.setText(totalString);
    }

    private void updateSplitNumber() {
        TextView splitIntoTextView = findViewById(R.id.textView_split_number);
        String splitIntoString = String.format("%d", splitIntoNumber);
        splitIntoTextView.setText(splitIntoString);
    }

    private void updateSplitAmount() {
        int splitAmountInCents = billTotalInCents / splitIntoNumber;

        // Add one cent to split amount if sum of split amounts is less than bill total
        // (to account for integer division)--better to overpay by a few cents instead
        // of shortchanging restaurant
        if (splitAmountInCents * splitIntoNumber < billTotalInCents) {
            splitAmountInCents++;
        }

        double splitAmount = (double) splitAmountInCents / 100.0;

        TextView splitAmountTextView = findViewById(R.id.textView_split_amount);
        String splitAmountString = String.format("$%,.2f", splitAmount);
        splitAmountTextView.setText(splitAmountString);
    }
}