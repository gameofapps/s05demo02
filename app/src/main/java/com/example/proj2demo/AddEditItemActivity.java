package com.example.proj2demo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AddEditItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_item);

        // Retrieve parameters from Main Activity
        Intent intent = getIntent();
        position = intent.getIntExtra("position", -1);

        setupActionBar();
        setupFields();
        setupButtons();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private int position = -1;
    private String itemName = null;
    private int itemPriceInCents = 0;
    private int itemTaxInCents = 0;
    private EditText itemNameEditText = null;
    private TextView itemNameErrorLabelTextView = null;
    private TextView itemNameErrorTextView = null;
    private EditText itemPriceEditText = null;
    private TextView itemPriceErrorLabelTextView = null;
    private TextView itemPriceErrorTextView = null;
    private TextView itemPriceDollarSignTextView = null;
    private EditText itemTaxEditText = null;
    private TextView itemTaxErrorLabelTextView = null;
    private TextView itemTaxErrorTextView = null;
    private TextView itemTaxDollarSignTextView = null;

    private void setupActionBar() {
        // Set title depending on whether we're adding or editing an item
        String title = (position == -1) ? "Add Item" : "Edit Item";
        setTitle(title);
    }

    private void setupFields() {
        itemNameEditText = findViewById(R.id.editText_item_name);
        itemNameErrorTextView = findViewById(R.id.textView_error_label_item_name);
        itemNameErrorLabelTextView = findViewById(R.id.textView_error_item_name);
        itemNameErrorTextView.setVisibility(View.INVISIBLE);
        itemNameErrorLabelTextView.setVisibility(View.INVISIBLE);

        itemPriceEditText = findViewById(R.id.editText_item_price);
        itemPriceErrorTextView = findViewById(R.id.textView_error_label_item_price);
        itemPriceErrorLabelTextView = findViewById(R.id.textView_error_item_price);
        itemPriceErrorTextView.setVisibility(View.INVISIBLE);
        itemPriceErrorLabelTextView.setVisibility(View.INVISIBLE);
        itemPriceDollarSignTextView = findViewById(R.id.textView_item_price_dollar_sign);

        itemTaxEditText = findViewById(R.id.editText_item_tax);
        itemTaxErrorTextView = findViewById(R.id.textView_error_label_item_tax);
        itemTaxErrorLabelTextView = findViewById(R.id.textView_error_item_tax);
        itemTaxErrorTextView.setVisibility(View.INVISIBLE);
        itemTaxErrorLabelTextView.setVisibility(View.INVISIBLE);
        itemTaxDollarSignTextView = findViewById(R.id.textView_item_tax_dollar_sign);

        if (position != -1) {
            fillInValues();
        }
    }

    private void fillInValues() {
        if ((position < 0) || (position >= Order.orders.size())) { return; }

        Order order = Order.orders.get(position);
        if ((order.name != null) && (!order.name.isEmpty())) {
            itemNameEditText.setText(order.name);
        }

        double price = Double.valueOf(order.priceInCents) / 100.0;
        String priceString = String.format("%,.2f", price);
        itemPriceEditText.setText(priceString);

        double tax = Double.valueOf(order.taxInCents) / 100.0;
        String taxString = String.format("%,.2f", tax);
        itemTaxEditText.setText(taxString);
    }

    private void setupButtons() {
        Button doneButton = findViewById(R.id.button_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean foundError = checkInputsAndShowError();
                if (!foundError) {
                    // Return name, price, tax to main activity
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("name", itemName);
                    resultIntent.putExtra("priceInCents", itemPriceInCents);
                    resultIntent.putExtra("taxInCents", itemTaxInCents);

                    if (position != -1) {
                        resultIntent.putExtra("position", position);
                    }

                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
            }
        });

        Button cancelButton = findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean checkInputsAndShowError() {
        boolean foundError = false;
        hideItemNameError();
        itemName = itemNameEditText.getText().toString();
        if ((itemName == null) || (itemName.isEmpty())) {
            foundError = true;
            showItemNameError();
        }

        hideItemPriceError();
        String itemPriceString = itemPriceEditText.getText().toString();
        if ((itemPriceString != null) && (!itemPriceString.isEmpty())) {
            try {
                String strippedItemPriceString = itemPriceString.replaceAll("[$,]", "");
                double itemPriceDouble = Double.parseDouble(strippedItemPriceString);
                itemPriceInCents = (int) (itemPriceDouble * 100.0);
            }
            catch (Exception e) {
                foundError = true;
                showItemPriceError();
            }
        }
        else {
            foundError = true;
            showItemPriceError();
        }

        hideItemTaxError();
        String itemTaxString = itemTaxEditText.getText().toString();
        if ((itemTaxString != null) && (!itemTaxString.isEmpty())) {
            try {
                String strippedItemTaxString = itemTaxString.replaceAll("[$,]", "");
                double itemTaxDouble = Double.parseDouble(strippedItemTaxString);
                itemTaxInCents = (int) (itemTaxDouble * 100.0);
            }
            catch (Exception e) {
                foundError = true;
                showItemTaxError();
            }
        }
        else {
            foundError = true;
            showItemTaxError();
        }

        return foundError;
    }

    private void showItemNameError() {
        itemNameErrorLabelTextView.setVisibility(View.VISIBLE);
        itemNameErrorTextView.setVisibility(View.VISIBLE);
    }

    private void hideItemNameError() {
        itemNameErrorLabelTextView.setVisibility(View.INVISIBLE);
        itemNameErrorTextView.setVisibility(View.INVISIBLE);
    }

    private void showItemPriceError() {
        itemPriceErrorLabelTextView.setVisibility(View.VISIBLE);
        itemPriceErrorTextView.setVisibility(View.VISIBLE);
        itemPriceDollarSignTextView.setTextColor(getResources().getColor(R.color.orangeCA5000));
    }

    private void hideItemPriceError() {
        itemPriceErrorLabelTextView.setVisibility(View.INVISIBLE);
        itemPriceErrorTextView.setVisibility(View.INVISIBLE);
        itemPriceDollarSignTextView.setTextColor(getResources().getColor(R.color.grey97));
    }

    private void showItemTaxError() {
        itemTaxErrorLabelTextView.setVisibility(View.VISIBLE);
        itemTaxErrorTextView.setVisibility(View.VISIBLE);
        itemTaxDollarSignTextView.setTextColor(getResources().getColor(R.color.orangeCA5000));
    }

    private void hideItemTaxError() {
        itemTaxErrorLabelTextView.setVisibility(View.INVISIBLE);
        itemTaxErrorTextView.setVisibility(View.INVISIBLE);
        itemTaxDollarSignTextView.setTextColor(getResources().getColor(R.color.grey97));
    }
}