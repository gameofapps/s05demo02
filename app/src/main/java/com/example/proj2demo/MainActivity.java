package com.example.proj2demo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up recycler view
        RecyclerView ordersList = findViewById(R.id.recyclerView_orders);
        ordersListAdapter = new OrdersListAdapter(Order.orders);
        ordersList.setAdapter(ordersListAdapter);
        ordersList.setLayoutManager(new LinearLayoutManager(this));
        ordersListAdapter.onClickOrdersList = new OrdersListAdapter.OnClickOrdersList() {
            @Override
            public void onClickOrder(int position, Order order) {
                if ((position < 0) || (position >= Order.orders.size())) { return; }
                showEditDeleteDialog(position);
            }
        };

        setupSplitButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.button_add_order) {
            navigateToAddScreen();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (ADD_EDIT_ITEM) : {
                if (resultCode == Activity.RESULT_OK) {
                    String name = data.getStringExtra("name");
                    int priceInCents = data.getIntExtra("priceInCents", 0);
                    int taxInCents = data.getIntExtra("taxInCents", 0);
                    int position = data.getIntExtra("position", -1);

                    int startingRow = 0;
                    if (position == -1) {
                        // Added a new item
                        Order order = new Order(name, priceInCents, taxInCents);
                        Order.orders.add(0, order);
                        ordersListAdapter.notifyItemInserted(0);
                        startingRow = 1;  // No need to refresh newly added row
                    }
                    else {
                        Order order = Order.orders.get(position);
                        order.name = name;
                        order.priceInCents = priceInCents;
                        order.taxInCents = taxInCents;
                    }

                    // Refresh remaining items so they show correct row numbers and/or updated values
                    for (int i=startingRow; i<Order.orders.size(); i++) {
                        ordersListAdapter.notifyItemChanged(i);
                    }

                    // Update total price and item count
                    displayTotals();
                }
                break;
            }
        }
    }

    private final int ADD_EDIT_ITEM = 100;
    private OrdersListAdapter ordersListAdapter = null;
    private int billTotalInCents = 0;

    private void navigateToAddScreen() {
        Intent intent = new Intent(this, AddEditItemActivity.class);
        startActivityForResult(intent, ADD_EDIT_ITEM);
    }

    private void navigateToEditScreen(int position) {
        Intent intent = new Intent(this, AddEditItemActivity.class);
        intent.putExtra("position", position);
        startActivityForResult(intent, ADD_EDIT_ITEM);
    }

    private void navigateToSplitBillScreen() {
        Intent intent = new Intent(this, SplitBillActivity.class);
        intent.putExtra("billTotalInCents", billTotalInCents);
        startActivity(intent);
    }

    private void showEditDeleteDialog(int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View dialogView = layoutInflater.inflate(R.layout.dialog_edit_delete, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        Button editButton = dialogView.findViewById(R.id.button_edit_item);
        Button deleteButton = dialogView.findViewById(R.id.button_delete_item);
        editButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                navigateToEditScreen(position);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                showConfirmDeleteDialog(position);
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private void showConfirmDeleteDialog(int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View dialogView = layoutInflater.inflate(R.layout.dialog_confirm_delete, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        Button backButton = dialogView.findViewById(R.id.button_go_back);
        Button deleteButton = dialogView.findViewById(R.id.button_delete);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Order.orders.remove(position);
                ordersListAdapter.notifyItemRemoved(position);
                displayTotals();
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private void displayTotals() {
        TextView itemCountTextView = findViewById(R.id.textView_number_items);
        String itemCountString = String.valueOf(Order.orders.size());
        if (Order.orders.size() == 1)  {
            itemCountString += " item";
        }
        else {
            itemCountString += " items";
        }
        itemCountTextView.setText(itemCountString);

        billTotalInCents = 0;
        for (int i = 0; i < Order.orders.size(); i++) {
            billTotalInCents += Order.orders.get(i).priceInCents;
            billTotalInCents += Order.orders.get(i).taxInCents;
        }
        double billTotal = Double.valueOf(billTotalInCents) / 100.0;
        String billTotalString = String.format("$%,.2f", billTotal);
        TextView totalTextView = findViewById(R.id.textView_dollar_amount);
        totalTextView.setText(billTotalString);
    }

    private void setupSplitButton() {
        Button splitButton = findViewById(R.id.button_split_bill);
        splitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToSplitBillScreen();
            }
        });
    }
}
